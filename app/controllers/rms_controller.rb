class RmsController < ApplicationController
  before_action :set_rm, only: [:show, :edit, :update, :destroy]

  def index
    @rms = Rm.all
  end

  def show
  end

  def new
    @rm = Rm.new
  end

  def edit
  end

  def create
    @rm = Rm.new(rm_params)

    respond_to do |format|
      if @rm.save
        format.html { redirect_to @rm, notice: 'Rm was successfully created.' }
        format.json { render :show, status: :created, location: @rm }
      else
        format.html { render :new }
        format.json { render json: @rm.errors, status: :unprocessable_entity }
      end
    end
  end

  def update
    respond_to do |format|
      if @rm.update(rm_params)
        format.html { redirect_to @rm, notice: 'Rm was successfully updated.' }
        format.json { render :show, status: :ok, location: @rm }
      else
        format.html { render :edit }
        format.json { render json: @rm.errors, status: :unprocessable_entity }
      end
    end
  end

  def destroy
    @rm.destroy
    respond_to do |format|
      format.html { redirect_to rms_url, notice: 'Rm was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    def set_rm
      @rm = Rm.find(params[:id])
    end

    def rm_params
      params.require(:rm).permit(:user_id, :content, :quantity, :max)
    end
end
